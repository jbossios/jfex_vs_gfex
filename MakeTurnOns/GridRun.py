#!/usr/bin/python
import os, sys
from time import strftime

test = False

timestamp = strftime("_%d%m%y")
if not test:
  if not os.path.exists("gridOutput"):
    os.system("mkdir gridOutput")
  if not os.path.exists("gridOutput/gridJobs"):
    os.system("mkdir gridOutput/gridJobs")

#config_name = "source/jetTriggerEfficiencies/data/config_DAOD_PhaseIIL1CaloTurnOns_L1_Emulate_useOfflineTrimmed_multijet.py"
#config_name = "source/jetTriggerEfficiencies/data/config_DAOD_PhaseIIL1CaloTurnOns_L1_Emulate_useOfflineTrimmed_singleModified.py"
config_name = "source/jetTriggerEfficiencies/data/config_DAOD_PhaseIL1CaloTurnOns_May21.py"
extraTag = "L1turnons_vs_offlineTrimmed_matchRates"

## Set this only for group production submissions ##
production_name = ""

samples = {
#  "ttbar": "valid1.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.recon.AOD.e4993_s3214_d1587_r12100_tid22623893_00", # Sep 2020 R21.3 validation
#  "ttbar" : "user.jbossios.450000.r11812_27102020_EXT0", # latest L1Calo simulation (r11812)
#  "HH4b_DAOD" : "user.jbossios.HH4b.DAOD_JETM1.r11812_24112020_fixed_EXT0", # latest L1Calo simulation (r11812) DAOD JETM1 including offline large-R trigmmed jets
  "HH4b_DAOD" : "user.fdelrio.450000.HHbbbb.DAOD_JETM1.r11812_EXT0", # latest L1Calo simulation (r11812) DAOD JETM1 including offline large-R trigmmed jets (from Fer)
}

#### Driver options ####
runType = 'grid'   #CERN grid

files = []
outputTags = []

##################################################################################
#
##################################################################################

for sampleName, sample in samples.iteritems():

  output_tag = sampleName + extraTag + timestamp
  submit_dir = "gridOutput/gridJobs/submitDir_"+output_tag

  ## Configure submission driver ##
  driverCommand = ''
  if runType == 'grid':
    #driverCommand = 'prun --optSubmitFlags="--forceStaged" --optGridOutputSampleName='
    #driverCommand = 'prun --optGridOutputSampleName='
    driverCommand = 'prun --optGridMaxNFilesPerJob=1 --optGridOutputSampleName='
    #driverCommand = 'prun --optGridMaxNFilesPerJob=2 --optSubmitFlags="--forceStaged" --optGridOutputSampleName='
    #driverCommand = 'prun --optSubmitFlags="--skipScout --excludedSite=ANALY_CERN_SHORT,ANALY_BNL_SHORT" --optGridOutputSampleName='
    if len(production_name) > 0:
      #driverCommand = ' prun --optSubmitFlags="--memory=5120 --official --skipScout" --optGridOutputSampleName='
      driverCommand = ' prun --optSubmitFlags="--official --forceStaged" --optGridOutputSampleName='
      #driverCommand = ' prun --optGridMaxNFilesPerJob=2 --optSubmitFlags="--official --forceStaged" --optGridOutputSampleName='
      #driverCommand = ' prun --optSubmitFlags="--official" --optGridOutputSampleName='
      driverCommand += 'group.'+production_name
    else:
      driverCommand += 'user.%nickname%'
    driverCommand += '.%in:name[2]%.'+output_tag
    #!driverCommand += '.%in:name[1]%.%in:name[2]%.'+output_tag
    #driverCommand += '.%in:name[1]%.%in:name[2]%.%in:name[3]%.'+output_tag
  elif runType == 'condor':
    driverCommand = ' condor --optFilesPerWorker 10 --optBatchWait'
  elif runType == 'local':
    driverCommand = ' direct'


  command = './source/xAODAnaHelpers/scripts/xAH_run.py'
  if runType == 'grid':
    command += ' --inputRucio '

  if 'sampleLists' in sample:
    command += ' --inputList'
  command += ' --files '+sample
  command += ' --config '+config_name
  command += ' --force --submitDir '+submit_dir
  command += ' '+driverCommand


  print command
  if not test: os.system(command)
