import ROOT
from xAODAnaHelpers import Config as xAH_config

c = xAH_config()

SingleJetChains = [
  'L1_J100',
  'L1_jJ100',
  'L1_gLJ100',
  'L1_gLJ120',
  'L1_gLJ140',
  'L1_gLJ160',
  'L1_gLJ180',
  'L1_jLJ100',
  'L1_jLJ120',
  'L1_jLJ140',
  'L1_jLJ160',
  'L1_jLJ180',
]

SingleTurnonString = ''
for chain in SingleJetChains:
  if SingleTurnonString != '': SingleTurnonString += '|'
  SingleTurnonString += chain
  #SingleTurnonString += '/j0_perf_L1RD0_FILLED'

msgLevel = "info"

c.algorithm("BasicEventSelection", { 
  "m_name"                    : "BasicSelection",
  "m_msgLevel"                : msgLevel,
  "m_applyGRLCut"             : False,
  "m_GRLxml"                  : "jetTriggerEfficiencies/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
  "m_derivationName"          : "EXOT2Kernel",
  "m_useMetaData"             : False,
  "m_storePassHLT"            : True,
  "m_storeTrigDecisions"      : True,
  "m_applyTriggerCut"         : True,
  "m_triggerSelection"        : "(HLT|L1)_[1-9]?0?(j|J)[0-9]+.*",
  "m_checkDuplicatesMC"       : True,
  "m_applyEventCleaningCut"   : False,
  "m_applyPrimaryVertexCut"   : False,
  "m_doPUreweighting"         : False,
  } )

## Calibrate offline small-R PFlow jets
#c.algorithm("JetCalibrator", {
#  "m_name"                    : "JetCalibrator",
#  "m_msgLevel"                : msgLevel,
#  "m_calibConfigFullSim"      : "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config",
#  "m_calibSequence"           : "JetArea_Residual_EtaJES_GSC_Smear",
#  "m_sort"                    : True,
#  "m_redoJVT"                 : True,
#  "m_inContainerName"         : "AntiKt4EMPFlowJets",
#  "m_outContainerName"        : "Jets_Calibrated",
#  "m_jetAlgo"                 : "AntiKt4EMPFlow",
#  "m_outputAlgo"              : "Jets_Calibrated_Algo",
#  "m_doCleaning"              : False,
#  "m_jetCleanUgly"            : False,
#  } )
## Select calibrated offline small-R PFlow jets
#c.algorithm("JetSelector", {
#  "m_name"                    : "JetSelector",
#  "m_msgLevel"                : msgLevel,
#  "m_decorateSelectedObjects" : False,
#  "m_cleanJets"               : True,
#  "m_doJVT"                   : True,
#  "m_noJVTVeto"               : False, # discard jets not passing JVT
#  "m_haveTruthJets"           : False,
#  "m_createSelectedContainer" : True,
#  "m_inContainerName"         : "Jets_Calibrated",
#  "m_outContainerName"        : "Jets_Selected",
#  } )

# Calibrate offline large-R trimmed jets
c.algorithm("JetCalibrator", {
  "m_name"                    : "JetCalibrator",
  "m_msgLevel"                : msgLevel,
  "m_calibConfigFullSim"      : "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_17Oct2018.config",
  "m_calibSequence"           : "EtaJES_JMS",
  "m_sort"                    : True,
  "m_redoJVT"                 : False,
  "m_inContainerName"         : "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
  "m_outContainerName"        : "Jets_Calibrated",
  "m_jetAlgo"                 : "AntiKt10LCTopoTrimmedPtFrac5SmallR20",
  "m_outputAlgo"              : "Jets_Calibrated_Algo",
  "m_doCleaning"              : False,
  "m_jetCleanUgly"            : False,
  "m_useLargeRTruthLabelingTool" : False,
  } )
# Select calibrated offline small-R PFlow jets
c.algorithm("JetSelector", {
  "m_name"                    : "JetSelector",
  "m_msgLevel"                : msgLevel,
  "m_decorateSelectedObjects" : False,
  "m_cleanJets"               : False,
  "m_doJVT"                   : False,
  "m_noJVTVeto"               : False, # discard jets not passing JVT
  "m_haveTruthJets"           : False,
  "m_createSelectedContainer" : True,
  "m_inContainerName"         : "Jets_Calibrated",
  "m_outContainerName"        : "Jets_Selected",
  } )

# Emulated turn-ons for unmodified chains
# inclusive nsubjets
c.algorithm("JetTriggerEfficiencies",   { 
  "m_name"                    : "JTE",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : SingleTurnonString,
  "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]" # The variable to plot as the x axis of the turno  
  } )

# Emulated turn-ons for unmodified chains
# nsubjets == 1
c.algorithm("JetTriggerEfficiencies",   { 
  "m_name"                    : "JTE_nsubjets1",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : SingleTurnonString,
  "m_selectionString"         : "auto+{'nsubjets':[0,2]}", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]" # The variable to plot as the x axis of the turno  
  } )

# Emulated turn-ons for unmodified chains
# nsubjets == 2
c.algorithm("JetTriggerEfficiencies",   { 
  "m_name"                    : "JTE_nsubjets2",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : SingleTurnonString,
  "m_selectionString"         : "auto+{'nsubjets':[1,3]}", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]" # The variable to plot as the x axis of the turno  
  } )


# Emulated turn-ons for unmodified chains
# nsubjets >= 3
c.algorithm("JetTriggerEfficiencies",   { 
  "m_name"                    : "JTE_nsubjets3",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : SingleTurnonString,
  "m_selectionString"         : "auto+{'nsubjets':[2,100]}", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]" # The variable to plot as the x axis of the turno  
  } )



# Emulated turn0ons for unmodified chains
c.algorithm("JetTriggerEfficiencies",   { 
  "m_name"                    : "JTE_multi",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : "L1_4jJ15|L1_4J15",
  "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[3]" # The variable to plot as the x axis of the turno  
  } )

# Now make turn ons changing the threshold for large jFEX (to compare with L1_gLJ100)

# nsubjets inclusive
c.algorithm("JetTriggerEfficiencies",   {
  "m_name"                    : "JTE_L1jLJ93",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : "L1_jLJ100",
  "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turnon
  "m_overrideThresholdL1"     : 93,
  } )

# nsubjets == 1
c.algorithm("JetTriggerEfficiencies",   {
  "m_name"                    : "JTE_L1jLJ93_nsubjets1",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : "L1_jLJ100",
  "m_selectionString"         : "auto+{'nsubjets':[0,2]}", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turnon
  "m_overrideThresholdL1"     : 93,
  } )

# nsubjets == 2
c.algorithm("JetTriggerEfficiencies",   {
  "m_name"                    : "JTE_L1jLJ93_nsubjets2",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : "L1_jLJ100",
  "m_selectionString"         : "auto+{'nsubjets':[1,3]}", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turnon
  "m_overrideThresholdL1"     : 93,
  } )

# nsubjets >= 3
c.algorithm("JetTriggerEfficiencies",   {
  "m_name"                    : "JTE_L1jLJ93_nsubjets3",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : "L1_jLJ100",
  "m_selectionString"         : "auto+{'nsubjets':[2,100]}", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turnon
  "m_overrideThresholdL1"     : 93,
  } )



