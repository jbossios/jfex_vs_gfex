import ROOT
from xAODAnaHelpers import Config as xAH_config

c = xAH_config()

SingleJetChains = {
  #'L1_J100'       : {'Emulate' : True,  'newThreshold' : '-1'},
  #'L1_jJ100'      : {'Emulate' : True,  'newThreshold' : '68'},
  #'L1_jLJ100'     : {'Emulate' : True,  'newThreshold' : '60'},
  #'L1_gLJ100'     : {'Emulate' : True,  'newThreshold' : '118'},
  #'L1_SC111-CJ15' : {'Emulate' : False, 'newThreshold' : '-1'},
  'L1_J100'       : {'Emulate' : True}, 
  'L1_jJ68'       : {'Emulate' : True},
  'L1_jLJ60'      : {'Emulate' : True},
  'L1_gLJ118'     : {'Emulate' : True},
  'L1_SC111-CJ15' : {'Emulate' : False},
}

msgLevel = "info"

c.algorithm("BasicEventSelection", { 
  "m_name"                    : "BasicSelection",
  "m_msgLevel"                : msgLevel,
  "m_applyGRLCut"             : False,
  "m_GRLxml"                  : "jetTriggerEfficiencies/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
  "m_derivationName"          : "EXOT2Kernel",
  "m_useMetaData"             : False,
  "m_storePassHLT"            : True,
  "m_storeTrigDecisions"      : True,
  "m_applyTriggerCut"         : True,
  "m_triggerSelection"        : "(HLT|L1)_[1-9]?0?(j|J)[0-9]+.*",
  "m_checkDuplicatesMC"       : True,
  "m_applyEventCleaningCut"   : False,
  "m_applyPrimaryVertexCut"   : False,
  "m_doPUreweighting"         : False,
  } )


# Calibrate offline large-R trimmed jets
c.algorithm("JetCalibrator", {
  "m_name"                    : "JetCalibrator",
  "m_msgLevel"                : msgLevel,
  "m_calibConfigFullSim"      : "JES_MC16recommendation_FatJet_Trimmed_JMS_comb_17Oct2018.config",
  "m_calibSequence"           : "EtaJES_JMS",
  "m_sort"                    : True,
  "m_redoJVT"                 : False,
  "m_inContainerName"         : "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets",
  "m_outContainerName"        : "Jets_Calibrated",
  "m_jetAlgo"                 : "AntiKt10LCTopoTrimmedPtFrac5SmallR20",
  "m_outputAlgo"              : "Jets_Calibrated_Algo",
  "m_doCleaning"              : False,
  "m_jetCleanUgly"            : False,
  "m_useLargeRTruthLabelingTool" : False,
  } )
# Select calibrated offline large-R trimmed jets
c.algorithm("JetSelector", {
  "m_name"                    : "JetSelector",
  "m_msgLevel"                : msgLevel,
  "m_decorateSelectedObjects" : False,
  "m_cleanJets"               : False,
  "m_doJVT"                   : False,
  "m_noJVTVeto"               : False, # discard jets not passing JVT
  "m_haveTruthJets"           : False,
  "m_createSelectedContainer" : True,
  "m_inContainerName"         : "Jets_Calibrated",
  "m_outContainerName"        : "Jets_Selected",
  } )

# TDT case
TDTstring = ''
for chain,Dict in SingleJetChains.items():
  if Dict['Emulate']: continue # continue chains that need to be emulated
  if TDTstring != '': TDTstring += '|'
  TDTstring += chain

# Emulation case
EmulateString = ''
for chain,Dict in SingleJetChains.items():
  if not Dict['Emulate']: continue # continue chains that need to be emulated
  if EmulateString != '': EmulateString += '|'
  EmulateString += chain

# TDT case

# nsubjets inclusive
c.algorithm("JetTriggerEfficiencies",   {
  "m_name"                    : "JTE_inclusive_TDT",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : True,
  "m_emulate"                 : False,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : TDTstring,
  "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turnon
  } )

# nsubjets == 1
c.algorithm("JetTriggerEfficiencies",   {
  "m_name"                    : "JTE_nsubjets1_TDT",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : True,
  "m_emulate"                 : False,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : TDTstring,
  "m_selectionString"         : "auto+{'nsubjets':[1,1]}", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turnon
  } )

# nsubjets == 2
c.algorithm("JetTriggerEfficiencies",   {
  "m_name"                    : "JTE_nsubjets2_TDT",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : True,
  "m_emulate"                 : False,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : TDTstring,
  "m_selectionString"         : "auto+{'nsubjets':[2,2]}", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turnon
  } )

# nsubjets >= 3
c.algorithm("JetTriggerEfficiencies",   {
  "m_name"                    : "JTE_nsubjets3_TDT",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : True,
  "m_emulate"                 : False,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : TDTstring,
  "m_selectionString"         : "auto+{'nsubjets':[3,-1]}", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turnon
  } )

# Emulation case

# nsubjets inclusive
c.algorithm("JetTriggerEfficiencies",   {
  "m_name"                    : "JTE_inclusive_Emulation",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : EmulateString,
  "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turnon
  } )

# nsubjets == 1
c.algorithm("JetTriggerEfficiencies",   {
  "m_name"                    : "JTE_nsubjets1_Emulation",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : EmulateString,
  "m_selectionString"         : "auto+{'nsubjets':[1,1]}", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turnon
  } )

# nsubjets == 2
c.algorithm("JetTriggerEfficiencies",   {
  "m_name"                    : "JTE_nsubjets2_Emulation",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : EmulateString,
  "m_selectionString"         : "auto+{'nsubjets':[2,2]}", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turnon
  } )

# nsubjets >= 3
c.algorithm("JetTriggerEfficiencies",   {
  "m_name"                    : "JTE_nsubjets3_Emulation",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : EmulateString,
  "m_selectionString"         : "auto+{'nsubjets':[3,-1]}", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turnon
  } )



