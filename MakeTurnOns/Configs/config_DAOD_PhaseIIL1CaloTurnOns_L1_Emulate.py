import ROOT
from xAODAnaHelpers import Config as xAH_config

c = xAH_config()

SingleJetChains = [
  'L1_J100',
  'L1_jJ100',
  'L1_gLJ100',
  'L1_gLJ120',
  'L1_gLJ140',
  'L1_gLJ160',
  'L1_gLJ180',
  'L1_jLJ100',
  'L1_jLJ120',
  'L1_jLJ140',
  'L1_jLJ160',
  'L1_jLJ180',
]

SingleTurnonString = ''
for chain in SingleJetChains:
  if SingleTurnonString != '': SingleTurnonString += '|'
  SingleTurnonString += chain
  #SingleTurnonString += '/j0_perf_L1RD0_FILLED'

msgLevel = "info"

c.algorithm("BasicEventSelection", { 
  "m_name"                    : "BasicSelection",
  "m_msgLevel"                : msgLevel,
  "m_applyGRLCut"             : False,
  "m_GRLxml"                  : "jetTriggerEfficiencies/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
  "m_derivationName"          : "EXOT2Kernel",
  "m_useMetaData"             : False,
  "m_storePassHLT"            : True,
  "m_storeTrigDecisions"      : True,
  "m_applyTriggerCut"         : True,
  "m_triggerSelection"        : "(HLT|L1)_[1-9]?0?(j|J)[0-9]+.*",
  "m_checkDuplicatesMC"       : True,
  "m_applyEventCleaningCut"   : False,
  "m_applyPrimaryVertexCut"   : False,
  "m_doPUreweighting"         : False,
  } )

# Calibrate offline small-R PFlow jets
c.algorithm("JetCalibrator", {
  "m_name"                    : "JetCalibrator",
  "m_msgLevel"                : msgLevel,
  "m_calibConfigFullSim"      : "JES_MC16Recommendation_Consolidated_PFlow_Apr2019_Rel21.config",
  "m_calibSequence"           : "JetArea_Residual_EtaJES_GSC_Smear",
  "m_sort"                    : True,
  "m_redoJVT"                 : True,
  "m_inContainerName"         : "AntiKt4EMPFlowJets",
  "m_outContainerName"        : "Jets_Calibrated",
  "m_jetAlgo"                 : "AntiKt4EMPFlow",
  "m_outputAlgo"              : "Jets_Calibrated_Algo",
  "m_doCleaning"              : False,
  "m_jetCleanUgly"            : False,
  } )
# Select calibrated offline small-R PFlow jets
c.algorithm("JetSelector", {
  "m_name"                    : "JetSelector",
  "m_msgLevel"                : msgLevel,
  "m_decorateSelectedObjects" : False,
  "m_cleanJets"               : True,
  "m_doJVT"                   : True,
  "m_noJVTVeto"               : False, # discard jets not passing JVT
  "m_haveTruthJets"           : False,
  "m_createSelectedContainer" : True,
  "m_inContainerName"         : "Jets_Calibrated",
  "m_outContainerName"        : "Jets_Selected",
  } )

# JetTriggerEfficiencies for single jet chains
c.algorithm("JetTriggerEfficiencies",   { 
  "m_name"                    : "JetTriggerEfficiencies",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : SingleTurnonString,
  "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]" # The variable to plot as the x axis of the turno  
  } )

# JetTriggerEfficiencies for multijet chains
c.algorithm("JetTriggerEfficiencies",   { 
  "m_name"                    : "JetTriggerEfficiencies_multi",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  #"m_turnonString"            : "HLT_6j45_L14jJ15/j0_perf_L1RD0_FILLED|HLT_6j45/j0_perf_L1RD0_FILLED",
  "m_turnonString"            : "L1_4jJ15|L1_4J15",
  "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[3]" # The variable to plot as the x axis of the turno  
  } )

# Now make turn ons changing the threshold for large jFEX (compare with L1_gLJ100)
c.algorithm("JetTriggerEfficiencies",   { 
  "m_name"                    : "JetTriggerEfficiencies_L1_jLJ93",
  "m_msgLevel"                : "info",
  "m_fromNTUP"                : False,
  "m_jetTriggerMenuSet"       : "2018",
  "m_offlineContainerName"    : "Jets_Selected", # the jet collection that will form the x axis of your turnons
  "m_TDT"                     : False,
  "m_emulate"                 : True,
  "m_requireTriggerInfoMatch" : False,
  "m_turnonString"            : "L1_jLJ100",
  "m_selectionString"         : "auto", # eg "auto | {'m': 45}"
  "m_variableString"          : "pt[0]", # The variable to plot as the x axis of the turnon
  "m_overrideThresholdL1"     : 93,
  } )

