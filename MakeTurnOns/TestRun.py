import os,sys

#sample = "/eos/atlas/atlascerngroupdisk/trig-jet/AOD_2020_September_Validation_R21_3/valid1/AOD.22623893._000502.pool.root.1"
#sample = "/afs/cern.ch/user/j/jbossios/work/public/JetTrigger/DeriveL1calibration/git/ProduceDAODs/runLocally/DAOD_JETM1.test.pool.root"
#sample = "/afs/cern.ch/user/j/jbossios/work/public/JetTrigger/DeriveL1calibration/git/ProduceDAODs_final/runLocally/DAOD_JETM1.test.pool.root"
sample = "/afs/cern.ch/user/j/jbossios/work/public/JetTrigger/DeriveL1calibration/git/ProduceDAODs_backup/runLocally/DAOD_JETM1.test.pool.root"

#config = "config_DAOD_PhaseIIL1CaloTurnOns.py"
#config = "config_DAOD_PhaseIIL1CaloTurnOns_L1.py"
#config = "config_DAOD_PhaseIIL1CaloTurnOns_L1_Emulate.py"
#config = "config_DAOD_PhaseIIL1CaloTurnOns_L1_Emulate_useOfflineTrimmed.py"
#config = 'config_DAOD_PhaseIIL1CaloTurnOns_L1_Emulate_useOfflineTrimmed_singleUnmodified.py'
#config = 'config_DAOD_PhaseIIL1CaloTurnOns_L1_Emulate_useOfflineTrimmed_singleModified.py'
config = 'config_DAOD_PhaseIL1CaloTurnOns_May21.py'

fromNTUP = False

################################################
## DO NOT MODIFY
################################################

# run xAH_run.py
command  = "python source/xAODAnaHelpers/scripts/xAH_run.py --config source/jetTriggerEfficiencies/data/"
command += config
command += " --files "
command += sample
if fromNTUP:
  command += " --treeName outTree/nominal"
command += " --force direct"
print(command)
os.system(command)
