mkdir source
cd source/
# get xAH
git clone git@github.com:UCATLAS/xAODAnaHelpers.git
# get jetTriggerEfficiencies
git clone ssh://git@gitlab.cern.ch:7999/atlas-trigger/jet/jetTriggerEfficiencies.git
# get jet-trigger-info
cd jetTriggerEfficiencies
git clone https://gitlab.cern.ch/atlas-trigger/jet/jet-trigger-details.git
cd jet-trigger-details
# dummy run to get a rulebook_hacked.py for cmake to learn about
python get_trigger_info.py --menuSet 2016 --trigger HLT_j510
# copy configs to jetTriggerEfficiencies/data
cd ../
mkdir data
cp ../../Configs/* data/
cd ../../
mkdir build/
