#################################################################################################
## Author:  Jona Bossio (jbossios@cern.ch)
## Date:    29/09/2020
## Purpose: Fill L1 jet distributions
#################################################################################################

# SetBranchStatus
def setBranchStatus(tree):
  tree.SetBranchStatus("*",0) # disable all branches
  tree.SetBranchStatus("mcEventWeight",1)
  tree.SetBranchStatus("mcChannelNumber",1)
  tree.SetBranchStatus("run2Jets_et8x8",1)
  tree.SetBranchStatus("run2Jets_eta",1)
  tree.SetBranchStatus("run2Jets_phi",1)
  tree.SetBranchStatus("jJets_et8x8",1)
  tree.SetBranchStatus("jJets_eta",1)
  tree.SetBranchStatus("jJets_phi",1)
  tree.SetBranchStatus("jLJets_et8x8",1)
  tree.SetBranchStatus("jLJets_eta",1)
  tree.SetBranchStatus("jLJets_phi",1)
  tree.SetBranchStatus("gLJets_et8x8",1)
  tree.SetBranchStatus("gLJets_eta",1)
  tree.SetBranchStatus("gLJets_phi",1)

# SetBranchAddress
import ROOT,array
def setBranchAddress(tree, Branches):
  Branches["mcEventWeight"] = array.array('f',[0])
  tree.SetBranchAddress("mcEventWeight",Branches["mcEventWeight"])
  Branches["mcChannelNumber"] = array.array('i',[0])
  tree.SetBranchAddress("mcChannelNumber",Branches["mcChannelNumber"])
  Branches["run2Jets_pt"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("run2Jets_et8x8",Branches["run2Jets_pt"])
  Branches["run2Jets_eta"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("run2Jets_eta",Branches["run2Jets_eta"])
  Branches["run2Jets_phi"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("run2Jets_phi",Branches["run2Jets_phi"])
  Branches["jJets_pt"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jJets_et8x8",Branches["jJets_pt"])
  Branches["jJets_eta"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jJets_eta",Branches["jJets_eta"])
  Branches["jJets_phi"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jJets_phi",Branches["jJets_phi"])
  Branches["jLJets_pt"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jLJets_et8x8",Branches["jLJets_pt"])
  Branches["jLJets_eta"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jLJets_eta",Branches["jLJets_eta"])
  Branches["jLJets_phi"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("jLJets_phi",Branches["jLJets_phi"])
  Branches["gLJets_pt"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("gLJets_et8x8",Branches["gLJets_pt"])
  Branches["gLJets_eta"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("gLJets_eta",Branches["gLJets_eta"])
  Branches["gLJets_phi"] = ROOT.std.vector('float')()
  tree.SetBranchAddress("gLJets_phi",Branches["gLJets_phi"])

# get DSID
def getDSID(path):
  if path.find("3610") != -1:
    return path[path.find("3610"):path.find("3610")+6]
  elif path.find("3129") != -1:
    return path[path.find("3129"):path.find("3129")+6]
  elif path.find("3461") != -1:
    return path[path.find("3461"):path.find("3461")+6]

from ROOT import *
import os,sys,argparse
from metadata_dict import MetadataFiles
from HelperClasses import *
from Arrays import *
from math import *
#import numpy as np

# Read arguments
parser = argparse.ArgumentParser()
parser.add_argument('--path',            action='store',      dest="path")
parser.add_argument('--outPATH',         action='store',      dest="outPATH")
parser.add_argument('--file',            action='store',      dest="inputFile")
parser.add_argument('--dataset',         action='store',      dest="dataset")
parser.add_argument('--debug',           action='store_true', dest="debug",       default=False)

args = parser.parse_args()

# Sample type
Dataset  = args.dataset

# Debugging
Debug = args.debug

# Protections
if Dataset not in DatasetOptions:
  print "ERROR: Dataset not recognised, exiting"
  sys.exit(0)
if args.path is None:
  print "ERROR: path to input file not provided, exiting"
  sys.exit(0)
if args.outPATH is None:
  print "ERROR: path to output files (outPATH) not provided, exiting"
  sys.exit(0)
if args.inputFile is None:
  print "ERROR: input file not provided, exiting"
  sys.exit(0)

#####################
# Output file name
#####################

inputFileName  = args.inputFile.replace(".root","")

OutName    = str(args.outPATH)
OutName   += "AllL1Hists_"
OutName   += Dataset
OutName   += "_" + inputFileName
if Debug:
  OutName += "_Debug"
OutName   += ".root"

#####################
# TTree name
#####################
TDirectoryName = "TreeAlgo"

# Max number of events for debuging
DebugMAXevents = 1000

###########################
# Get sumWeights (MC only)
###########################
sumWeights = dict()
# Loop over metadata files
if Dataset not in MetadataFiles:
  print "ERROR: No metadata found for "+Dataset+", exiting"
  sys.exit(0)
for key,value in MetadataFiles[Dataset].items():
  # open metadata file
  MetadataFile = TFile.Open(value)
  if not MetadataFile:
    print "ERROR: "+value+" file not found, exiting"
    sys.exit(0)
  # get histogram
  MetadataHist    = MetadataFile.Get("cutflow")
  sumWeights[key] = MetadataHist.GetBinContent(0)
  MetadataFile.Close()

###############################
# Read AMI info (if requested)
###############################
FileName = "ami_reader.txt"
AMIFile  = open(FileName,'r')
lines    = AMIFile.readlines()
AMIxss = dict() # cross section values
AMIeff = dict() # efficiency values
AMIk   = dict() # k-factor values
for line in lines: # loop over lines
  Line = line.split(" ")
  dsid = int(Line[0]) # first column is dsid
  AMIxss[dsid] = float(Line[1]) # second column is cross section
  AMIeff[dsid] = float(Line[2]) if '\n' not in Line[2] else float(Line[2][:-2]) # third column is efficiency
  if len(Line) > 3: kFactor = Line[3] # fourth column (if available) is k-factor
  else: kFactor = '1.0'
  AMIk[dsid] = float(kFactor) if '\n' not in kFactor else float(kFactor)

#############################################
# Open TFile/TDirectory and loop over events
#############################################
print "INFO: Opening "+args.path+args.inputFile
tfile = TFile.Open(args.path+args.inputFile)
if not tfile:
  print "ERROR: "+args.path+args.inputFile+" not found, exiting"
  sys.exit(0)

tdir = TDirectoryFile()
tdir = tfile.Get(TDirectoryName)
if not tdir:
  print "ERROR: "+TDirectoryName+" not found, exiting"
  sys.exit(0)

# Get list of TTrees
Keys = tdir.GetListOfKeys()
keys = []
for key in Keys: keys.append(key.GetName())

# Set list of requested TTrees
keys = ['nominal']

# Loop over TTrees
for TTreeName in keys:

  # Open TFile
  print "INFO: Opening "+args.path+args.inputFile
  tfile = TFile.Open(args.path+args.inputFile)
  if not tfile:
    print "ERROR: "+args.path+args.inputFile+" not found, exiting"
    sys.exit(0)

  # Open corresponding directory
  tdir = TDirectoryFile()
  tdir = tfile.Get(TDirectoryName)
  if not tdir:
    print "ERROR: "+TDirectoryName+" not found, exiting"
    sys.exit(0)

  # Get TTree
  if Debug: print "DEBUG: Get "+TTreeName+" TTree"
  tree = tdir.Get(TTreeName)
  if not tree:
    print "ERROR: "+TTreeName+" not found, exiting"
    sys.exit(0)
  totalEvents = tree.GetEntries()
  print "INFO: TotalEvents in "+TTreeName+": "+str(totalEvents)

  ###################
  # Book histograms
  ###################

  Histograms = dict()
  # Loop over jet collections
  for jetcoll in ['run2Jets','jJets','jLJets','gLJets']:
    for i in range(0,8): # loop over first 8 leading jets
      name = '{}Pt{}'.format(jetcoll,i)
      hist = TH1D(name,'',7000,1,7001)
      hist.Sumw2()
      Histograms[name] = hist

  ################
  # Set branches
  ################
  Branches = dict()
  setBranchStatus(tree)
  setBranchAddress(tree,Branches)

  ###################
  # Loop over events
  ###################
  for event_counter in xrange(0,totalEvents):

    # Get entry
    tree.GetEntry(event_counter)

    event_counter += 1
    if Debug and event_counter > DebugMAXevents:
      break # skip loop
    if Debug:
      print "DEBUG: Event counter: "+str(event_counter)
    if event_counter == 1:
      print "INFO: Running on event #"+str(event_counter)+" of "+str(totalEvents)+" events"
    if event_counter % 100000 == 0:
      print "INFO: Running on event #"+str(event_counter)+" of "+str(totalEvents)+" events"
 
    ####################
    # Get sample weight
    ####################
    wgt  = Branches["mcEventWeight"][0]
    wgt *= AMIxss[Branches["mcChannelNumber"][0]]
    wgt *= AMIeff[Branches["mcChannelNumber"][0]]
    wgt *= AMIk[Branches["mcChannelNumber"][0]]
    wgt /= sumWeights[Branches["mcChannelNumber"][0]]
    if Debug:
      print "DEBUG: mcEventWeight: "+str(Branches["mcEventWeight"][0])
      print "DEBUG: final weight: "+str(wgt)

    ############################
    # Loop over jet collections
    ############################
    for jetcoll in ['run2Jets','jJets','jLJets','gLJets']:

      #############################
      # Select L1 jets
      #############################
      SelectedL1Jets = []
      if Debug: print('Select L1 {} jets:'.format(jetcoll))
      for ijet in xrange(0,len(Branches['{}_pt'.format(jetcoll)])):
        jetPt  = Branches['{}_pt'.format(jetcoll)][ijet]
        jetEta = Branches['{}_eta'.format(jetcoll)][ijet]
        jetPhi = Branches['{}_phi'.format(jetcoll)][ijet]
        if Debug: print('DEBUG: L1 jet pt = {}'.format(jetPt))
        # protection
        if jetPt < 0: continue # skip negative et8x8 L1 jet
        if abs(jetEta) > 3.1: continue # skip forward L1 jets
        TLVjet = iJet()
        TLVjet.SetPtEtaPhiM(jetPt,jetEta,jetPhi,0) # L1 jets are massless
        SelectedL1Jets.append(TLVjet)
      if Debug: print('DEBUG: Number of selected L1 jets = {}'.format(len(SelectedNoCalibL1Jets)))
      if Debug: print('DEBUG: Selected L1 jets:')
      for jet in SelectedL1Jets:
        if Debug: print('DEBUG: Jet pt = {}'.format(jet.Pt()))

      #############################################
      # Fill distributions
      #############################################
      if Debug: print('DEBUG: Filling distributions')
      for ijet in range(0,len(SelectedL1Jets)):
        if ijet > 7: continue
        Histograms['{}Pt{}'.format(jetcoll,ijet)].Fill(SelectedL1Jets[ijet].Pt())

  ##################################
  # Write histograms to a ROOT file
  ##################################
  outFile = TFile(OutName,"UPDATE")
  for key,hist in Histograms.iteritems():
    hist.Write()
  outFile.Close()
  print "Histograms saved to "+OutName
print ">>> DONE <<<"
