import ROOT
import os,sys

File = ROOT.TFile.Open('/eos/atlas/atlascerngroupdisk/trig-jet/Jona/HIST_AllL1Jets_JZ0/AllL1Hists_JZ0.root')
if not File:
  print('ERROR: AllL1Hists_JZ0.root not found, exiting')
  sys.exit(0)

# Chains
# J100
# jJX
# jLJX
# gLJX

# First: find #events for L1_J100 (Run 2 L1 chain)
Hist       = File.Get('run2JetsPt0')
minBin     = Hist.FindBin(100)
RefNevents = Hist.Integral(minBin,Hist.GetNbinsX())
print('Number of events triggered by L1_J100: {}'.format(RefNevents))

# Find threshold for each Phase-I chain for which the number of fired events matches those for L1_J100
Thresholds = dict()
for jetcoll in ['jJets','jLJets','gLJets']:
  print('Find optimal threshold for {}'.format(jetcoll))
  # Get Histogram
  Hist = File.Get('{}Pt0'.format(jetcoll))
  # Get number of fired events for several thresholds
  Nevents = dict()
  for th in range(50,151):
    minBin      = Hist.FindBin(th)
    Nevents[th] = Hist.Integral(minBin,Hist.GetNbinsX())
    #print('Nevents for {} = {}'.format(th,Nevents[th]))
  # Find optimal threshold
  maxDiff = 1e6
  for th in Nevents:
    diff = abs(Nevents[th]-RefNevents)
    if diff < maxDiff:
      maxDiff             = diff
      Thresholds[jetcoll] = th
  print('Number of events trigged by {} with threshold {} = {}'.format(jetcoll,Thresholds[jetcoll],Nevents[Thresholds[jetcoll]]))

for jetcoll in ['jJets','jLJets','gLJets']:
  print('Threshold for {} = {}'.format(jetcoll,Thresholds[jetcoll]))



