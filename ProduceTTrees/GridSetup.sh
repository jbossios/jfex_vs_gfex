#! /bin/bash

setupATLAS
cd source
lsetup "asetup AnalysisBase,21.2.139" panda
voms-proxy-init -voms atlas 
cd ../build
cmake ../source
make
source */setup.sh
cd ..
