import os,sys

#sample = '/eos/atlas/atlascerngroupdisk/trig-jet/AOD_jFEXsamples/mc16_13TeV/AOD.21577847._000775.pool.root.1'
sample = '/eos/atlas/atlascerngroupdisk/trig-jet/AOD_Dijets_JZ0_TestFiles4PhaseIstudies/user.jbossios/user.jbossios.23731822.EXT0._000010.AOD.root'

config = "config_Tree_AllL1Jets"

################################################
## DO NOT MODIFY
################################################

# choose appropiate config
CONFIG  = config
CONFIG += ".py"

# run xAH_run.py
command  = "python source/xAODAnaHelpers/scripts/xAH_run.py --config source/xAODAnaHelpers/data/"
command += CONFIG
command += " --files "
command += sample
command += " --force direct"
print(command)
os.system(command)
