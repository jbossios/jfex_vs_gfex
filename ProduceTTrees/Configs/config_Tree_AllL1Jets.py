import ROOT
from xAODAnaHelpers import Config as xAH_config

c = xAH_config()

msgLevel = "info"

# Make basic event selections
c.algorithm("BasicEventSelection", {
  "m_name"                        : "BasicSelection",
  "m_msgLevel"                    : msgLevel,
  "m_applyGRLCut"                 : False,
  "m_useMetaData"                 : True,
  "m_storePassHLT"                : False,
  "m_storeTrigDecisions"          : False,
  "m_applyTriggerCut"             : False,
  "m_triggerSelection"            : "",
  "m_checkDuplicatesMC"           : True,
  "m_applyJetCleaningEventFlag"   : False,
  "m_PVNTrack"                    : 2,
  "m_applyPrimaryVertexCut"       : False,
  "m_vertexContainerName"         : "PrimaryVertices",
  "m_doPUreweighting"             : False,
  } )

# Save L1 and truth jets to a TTree
c.algorithm("TreeAlgo", {
  "m_name"                        : "TreeAlgo",
  "m_msgLevel"                    : msgLevel,
  "m_l1JetContainerName"          : "LVL1JetRoIs jRoundJetsPUsub jRoundLargeRJetsPUsub gL1Jets",
  "m_l1JetBranchName"             : 'run2Jets jJets jLJets gLJets'
  } )

