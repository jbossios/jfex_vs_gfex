mcSamples = dict()

mcSamples['Dijets'] = [
# Old production with old L1Calo sim
#  'user.jbossios.361020.r11328_EXT0',
#  'user.jbossios.361021.r11328_EXT0',
#  'user.jbossios.361022.r11328_EXT0',
#  'user.jbossios.361023.r11328_EXT0',
#  'user.jbossios.361024.r11328_EXT0',
# Latest production with latest L1Calo sim
#  'user.jbossios.361024.r11812_27102020_EXT0',
#  'user.jbossios.361023.r11812_27102020_EXT0',
#  'user.jbossios.361022.r11812_27102020_EXT0',
#  'user.jbossios.361021.r11812_27102020_EXT0',
#  'user.jbossios.361020.r11812_27102020_EXT0',
# Production submitted on 7/1/2021
#  'user.jbossios.361024.AOD.r11812_07012021_EXT0/',
#  'user.jbossios.361023.AOD.r11812_07012021_EXT0/',
#  'user.jbossios.361022.AOD.r11812_07012021_EXT0/',
#  'user.jbossios.361021.AOD.r11812_07012021_EXT0/',
  'user.jbossios.361020.AOD.r11812_07012021_EXT0/',
]
